export async function getCategories() {
  return [
    {
      name: "Desk and Office",
      description: "Work from home accessories",
      imageUrl:
        "https://tailwindui.com/img/ecommerce-images/home-page-02-edition-01.jpg",
      imageCaption:
        "Desk with leather desk pad, walnut desk organizer, wireless keyboard and mouse, and porcelain mug.",
      href: "#",
    },
    {
      name: "Self-Improvement",
      description: "Journals and note-taking",
      imageUrl:
        "https://tailwindui.com/img/ecommerce-images/home-page-02-edition-02.jpg",
      imageCaption:
        "Wood table with porcelain mug, leather journal, brass pen, leather key ring, and a houseplant.",
      href: "#",
    },
    {
      name: "Travel",
      description: "Daily commute essentials",
      imageUrl:
        "https://tailwindui.com/img/ecommerce-images/home-page-02-edition-03.jpg",
      imageCaption:
        "Collection of four insulated travel bottles on wooden shelf.",
      href: "#",
    },
  ];
}
