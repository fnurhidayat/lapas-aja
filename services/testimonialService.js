import faker from "faker";

export async function getTestimonials() {
  return [
    {
      id: 1,
      name: faker.name.findName(),
      position: faker.name.jobTitle(),
      heading: faker.lorem.words(),
      paragraph: faker.lorem.paragraph(),
      reaction: "😍",
      imageUrl:
        "https://images.unsplash.com/photo-1542740348-39501cd6e2b4?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
      imageCaption: "Testimonee",
    },
    {
      id: 2,
      name: faker.name.findName(),
      position: faker.name.jobTitle(),
      heading: faker.lorem.words(),
      paragraph: faker.lorem.paragraph(),
      reaction: "😃",
      imageUrl:
        "https://images.unsplash.com/photo-1597586124394-fbd6ef244026?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
      imageCaption: "Testimonee",
    },
    {
      id: 3,
      name: faker.name.findName(),
      position: faker.name.jobTitle(),
      heading: faker.lorem.words(),
      paragraph: faker.lorem.paragraph(),
      reaction: "😡",
      imageUrl:
        "https://images.unsplash.com/photo-1605406575497-015ab0d21b9b?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
      imageCaption: "Testimonee",
    },
  ];
}
