export async function getProducts() {
  return [
    {
      id: 1,
      name: "Basic Tee",
      description: "Random black tee shirt.",
      href: "#",
      imageUrl:
        "https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-01.jpg",
      imageCaption: "Front of men's Basic Tee in black.",
      code: "TEE-001",
    },
    {
      id: 2,
      name: "Basic Tee",
      description: "Random aspen white tee shirt.",
      href: "#",
      imageUrl:
        "https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-02.jpg",
      imageCaption: "Front of men's Basic Tee in aspen white.",
      code: "TEE-002",
    },
    {
      id: 3,
      name: "Basic Tee",
      description: "Random charcoal tee shirt.",
      href: "#",
      imageUrl:
        "https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-03.jpg",
      imageCaption: "Front of men's Basic Tee in charcoal.",
      code: "TEE-003",
    },
    {
      id: 4,
      name: "Artwork Tee",
      description: "Random iso dots tee shirt.",
      href: "#",
      imageUrl:
        "https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-04.jpg",
      imageCaption: "Front of woman's Artwork Tee in iso dots.",
      code: "TEE-004",
    },
  ];
}
