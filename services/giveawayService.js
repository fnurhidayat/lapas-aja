export async function getLatestGiveaway() {
  return {
    id: 1,
    title: "I have this old weird thing, any taker?",
    description:
      "I went to my grandpa's old warehouse, you know looking for some family artifact then I found this little cute thing. I don't what it is, but it seems valuable, and we don't need this anymore. ",
    images: [
      {
        url: "https://tailwindui.com/img/ecommerce-images/product-feature-03-detail-01.jpg",
        caption:
          "Walnut card tray with white powder coated steel divider and 3 punchout holes.",
      },
      {
        url: "https://tailwindui.com/img/ecommerce-images/product-feature-03-detail-02.jpg",
        caption:
          "Top down view of walnut card tray with embedded magnets and card groove.",
      },
      {
        url: "https://tailwindui.com/img/ecommerce-images/product-feature-03-detail-03.jpg",
        caption:
          "Side of walnut card tray with card groove and recessed card area.",
      },
      {
        url: "https://tailwindui.com/img/ecommerce-images/product-feature-03-detail-04.jpg",
        caption:
          "Walnut card tray filled with cards and card angled in dedicated groove.",
      },
    ],
  };
}
