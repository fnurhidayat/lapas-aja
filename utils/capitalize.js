export function capitalize(value = "") {
    const text = value.split("");
    text[0] = text[0]?.toUpperCase();
    return text.join("");
}