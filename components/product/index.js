function Product({ children }) {
  const image = children[0];
  const name = children[1];
  const description = children[2];
  const code = children[3];

  return (
    <div className="group relative">
      <div className="w-full min-h-80 bg-gray-200 aspect-w-1 aspect-h-1 rounded-md overflow-hidden group-hover:opacity-75 lg:h-80 lg:aspect-none">
        {image}
      </div>
      <div className="mt-4 flex justify-between">
        <div>
          {name}
          {description}
        </div>
        {code}
      </div>
    </div>
  );
}

Product.Image = function ProductImage({ src, alt }) {
  return (
    <img
      src={src}
      alt={alt}
      className="w-full h-full object-center object-cover lg:w-full lg:h-full"
    />
  );
};

Product.Name = function ProductName({ href, children }) {
  return (
    <h3 className="text-sm text-gray-700">
      <a href={href}>
        <span aria-hidden="true" className="absolute inset-0" />
        {children}
      </a>
    </h3>
  );
};

Product.Description = function ProductDescription({ children }) {
  return <p className="mt-1 text-sm text-gray-500">{children}</p>;
};

Product.Code = function ProductCode({ children }) {
  return <p className="text-sm font-medium text-gray-900">{children}</p>;
};

export default Product;
