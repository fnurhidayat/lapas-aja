function Category({ children }) {
  const name = children[0];
  const description = children[1];
  const image = children[2];

  return (
    <div className="group relative">
      {image}
      {name}
      {description}
    </div>
  );
}

Category.Name = function CategoryName({ href, children }) {
  return (
    <h3 className="mt-6 text-sm text-gray-500">
      <a href={href}>
        <span className="absolute inset-0" />
        {children}
      </a>
    </h3>
  );
};

Category.Description = function CategoryDescription({ children }) {
  return <p className="text-base font-semibold text-gray-900">{children}</p>;
};

Category.Image = function CategoryImage({ src, alt }) {
  return (
    <div className="relative w-full h-80 bg-white rounded-lg overflow-hidden group-hover:opacity-75 sm:aspect-w-2 sm:aspect-h-1 sm:h-64 lg:aspect-w-1 lg:aspect-h-1">
      <img
        src={src}
        alt={alt}
        className="w-full h-full object-center object-cover"
      />
    </div>
  );
};

export default Category;
