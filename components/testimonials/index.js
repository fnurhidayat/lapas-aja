import React, { Fragment } from "react";
import {
  CarouselProvider,
  Slider,
  Slide,
  ButtonBack,
  ButtonNext,
} from "pure-react-carousel";
import classNames from "classnames";

/* Install pure-react-carousel using -> npm i pure-react-carousel */
function Testimonials({ children }) {
  const header = children[0];
  const testimonials = children[1];

  return (
    <div>
      <div className="flex items-center justify-between h-full w-full absolute z-1">
        <div className="w-1/3 bg-white h-full" />
        <div className="w-4/6 ml-16 bg-gray-100 h-full" />
      </div>
      <div className="xl:px-20 px-8 py-20 2xl:mx-auto 2xl:container relative z-2">
        <CarouselProvider
          naturalSlideWidth={100}
          isIntrinsicHeight={true}
          totalSlides={testimonials.length}
        >
          {header}
          <Slider>
            {testimonials.map((testimonial, index) => (
              <Slide index={index} key={index} tabIndex={index === 0 ? "null" : undefined}>
                <div
                  className={classNames("flex", index > 0 && "relative")}
                  style={{
                    transform: index > 0 ? "transformX(0%)" : undefined,
                  }}
                >
                  {testimonial}
                </div>
              </Slide>
            ))}
          </Slider>
          <div className="flex items-center mt-8">
            <ButtonBack
              className="cursor-pointer "
              role="button"
              aria-label="previous slide"
            >
              <img
                src="https://tuk-cdn.s3.amazonaws.com/can-uploader/testimonal-svg2.svg"
                alt="previous"
              />
            </ButtonBack>

            <ButtonNext
              role="button"
              aria-label="next slide"
              className="cursor-pointer ml-2"
            >
              <img
                src="https://tuk-cdn.s3.amazonaws.com/can-uploader/testimonial-svg3.svg"
                alt="next"
              />
            </ButtonNext>
          </div>
        </CarouselProvider>
      </div>
    </div>
  );
}

Testimonials.Header = function TestimonialsHeader({ children }) {
  return (
    <Fragment>
      <h1 className="text-5xl font-bold xl:block hidden leading-tight text-gray-800">
        {children}
      </h1>
      <h1 className="text-5xl font-bold xl:hidden block leading-tight lg:leading-10 text-gray-800">
        {children}
      </h1>
    </Fragment>
  );
};

export default Testimonials;
