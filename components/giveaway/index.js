/* This example requires Tailwind CSS v2.0+ */

function Giveaway({ children }) {
  const displayComponent = children[children.length - 1];
  const descriptionComponent = children.slice(0, children.length - 2);

  return (
    <div className="bg-white">
      <div className="max-w-2xl mx-auto py-24 px-4 grid items-center grid-cols-1 gap-y-16 gap-x-8 sm:px-6 sm:py-32 lg:max-w-7xl lg:px-8 lg:grid-cols-2">
        {displayComponent}
        <div>
          <h5 className="text-xl font-bold tracking-tight text-red-600 sm:text-xl">
            Latest Giveaway
          </h5>

          {descriptionComponent}
        </div>
      </div>
    </div>
  );
}

Giveaway.Title = function GiveawayTitle({ children }) {
  return (
    <h2 className="mt-4 text-3xl font-extrabold tracking-tight text-gray-900 sm:text-4xl">
      {children}
    </h2>
  );
};

Giveaway.Description = function GiveawayDescription({ children }) {
  return <p className="mt-4 text-gray-500">{children}</p>;
};

Giveaway.Control = function GiveawayControl({ children }) {
  return (
    <div className="mt-5 sm:mt-8 sm:flex sm:justify-center lg:justify-start">
      {children}
    </div>
  );
};

Giveaway.Display = function GiveawayDisplay({ children }) {
  return (
    <div className="grid grid-cols-2 grid-rows-2 gap-4 sm:gap-6 lg:gap-8">
      {children}
    </div>
  );
};

Giveaway.Display.Image = function GiveawayDisplayImage({ src, alt }) {
  return <img src={src} alt={alt} className="bg-gray-100 rounded-lg" />;
};

export default Giveaway;
