import { Fragment } from "react";

function Testimonial({ children = [] }) {
    const image = children[0];
    const reaction = children[1];
    const header = children[2];
    const paragraph = children[3];
    const name = children[4];
    const position = children[5];

    return (
        <div className="mt-14 md:flex">
            <div className="relative lg:w-1/2 sm:w-96 xl:h-96 h-80">
                {image}
                {reaction}
            </div>
            <div className="md:w-1/3 lg:w-1/3 xl:ml-32 md:ml-20 md:mt-0 mt-4 flex flex-col justify-between">
                <div>
                    {header}
                    {paragraph}
                </div>
                <div className="md:mt-0 mt-8">
                    {name}
                    {position}
                </div>
            </div>
        </div>
    );
}

Testimonial.Image = function TestimonialImage({ src, alt }) {
    return (
        <img
            src={src}
            alt={alt}
            className="w-full h-full flex-shrink-0 object-fit object-cover shadow-lg rounded"
        />
    );
};

Testimonial.Reaction = function TestimonialReaction({ children }) {
    return (
        <div className="w-32 md:flex hidden items-center justify-center absolute top-0 -mr-16 -mt-14 right-0 h-32 bg-red-100 rounded-full text-3xl">
            {children}
        </div>
    )
}

Testimonial.Header = function TestimonialHead({ children }) {
    return (
        <h1 className="text-2xl font-semibold xl:leading-loose text-gray-800">
            {children}
        </h1>
    );
};

Testimonial.Paragraph = function TestimonialParagraph({ children }) {
    return (
        <p className="text-base font-medium leading-6 mt-4 text-gray-600">
            {children}
        </p>
    );
};

Testimonial.Name = function TestimonialName({ children }) {
    return (
        <p className="text-base font-medium leading-4 text-gray-800">{children}</p>
    );
};

Testimonial.Position = function TestimonialPosition({ children }) {
    return (
        <p className="text-base leading-4 mt-2 mb-4 text-gray-600">{children}</p>
    );
};

export default Testimonial;
