import classNames from "classnames";

function getButtonBG(variant, intensity) {
  return `bg-${variant}-${intensity}`;
}

function getHoverButtonBG(variant, intensity) {
  return `hover:${getButtonBG(variant, intensity + 100)}`;
}

export default function Button({ children, className, ...props }) {
  return (
    <a
      className={classNames(
        "w-full",
        "flex",
        "items-center",
        "justify-center",
        "px-8",
        "py-3",
        "border",
        "border-transparent",
        "text-base",
        "font-medium",
        "rounded-md",
        "md:py-4",
        "md:text-lg",
        "md:px-10",
        className
      )}
      {...props}
    >
      {children}
    </a>
  );
}
