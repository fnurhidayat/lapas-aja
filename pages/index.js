import { Fragment } from "react";
import Hero from ":components/hero";
import Giveaway from ":components/giveaway";
import Button from ":components/button";
import Categories from ":components/categories";
import Category from ":components/category";
import Products from ":components/products";
import Product from ":components/product";
import Testimonials from ":components/testimonials";
import Testimonial from ":components/testimonial";
import Footer from ":components/footer";
import {
  getCategories,
  getLatestGiveaway,
  getProducts,
  getTestimonials,
} from ":services";
import { capitalize } from ":utils"

export default function Home({
  categories,
  latestGiveaway,
  products,
  testimonials,
}) {
  return (
    <Fragment>
      <Hero imageSrc="https://images.unsplash.com/photo-1532629345422-7515f3d16bb6?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80">
        <Hero.Title>
          <span className="block xl:inline">Looking for</span>{" "}
          <span className="block text-red-600 xl:inline">some goodies?</span>
        </Hero.Title>
        <Hero.Description>
          Take a look around, you might find what you need. Or if you&aposre
          interested to give your stuff away, go ahead, man.
        </Hero.Description>
        <Hero.Control>
          <div className="rounded-md shadow">
            <Button href="#" className="bg-red-500 hover:bg-red-700 text-white">
              Getting Started
            </Button>
          </div>
        </Hero.Control>
      </Hero>

      <Giveaway>
        <Giveaway.Title>{latestGiveaway.title}</Giveaway.Title>
        <Giveaway.Description>
          {latestGiveaway.description}
        </Giveaway.Description>

        <Giveaway.Control>
          <Button
            href="#"
            className="text-white bg-red-500 hover:bg-red-700 max-w-xs"
          >
            Join a Giveaway
          </Button>
        </Giveaway.Control>

        <Giveaway.Display>
          {latestGiveaway.images.map((i, index) => (
            <Giveaway.Display.Image key={index} src={i.url} alt={i.caption} />
          ))}
        </Giveaway.Display>
      </Giveaway>

      <Categories>
        <Categories.Header>Goodies Collection</Categories.Header>
        {categories.map((category, index) => (
          <Category key={index}>
            <Category.Name>{category?.name}</Category.Name>
            <Category.Description>{category.description}</Category.Description>
            <Category.Image
              src={category.imageUrl}
              alt={category.imageCaption}
            />
          </Category>
        ))}
      </Categories>

      <Products>
        <Products.Header>
          Someone wants to donate this, it probably might be yours!
        </Products.Header>
        {products.map((product) => (
          <Product key={product.id}>
            <Product.Image src={product.imageUrl} alt={product.imageCaption} />
            <Product.Name>{product.name}</Product.Name>
            <Product.Description>{product.description}</Product.Description>
            <Product.Code>{product.code}</Product.Code>
          </Product>
        ))}
      </Products>

      <Testimonials>
        <Testimonials.Header>
          A wholesome story <br></br> from our user
        </Testimonials.Header>
        {testimonials.map((testimonial) => (
          <Testimonial key={testimonial.id}>
            <Testimonial.Image
              src={testimonial.imageUrl}
              alt={testimonial.imageCaption}
            />
            <Testimonial.Reaction>{testimonial.reaction}</Testimonial.Reaction>
            <Testimonial.Header>{capitalize(testimonial.heading)}</Testimonial.Header>
            <Testimonial.Paragraph>
              {testimonial.paragraph}
            </Testimonial.Paragraph>
            <Testimonial.Name>{testimonial.name}</Testimonial.Name>
            <Testimonial.Position>{testimonial.position}</Testimonial.Position>
          </Testimonial>
        ))}
      </Testimonials>

      <Footer></Footer>
    </Fragment>
  );
}

export async function getServerSideProps() {
  const [latestGiveaway, categories, products, testimonials] =
    await Promise.all([
      getLatestGiveaway(),
      getCategories(),
      getProducts(),
      getTestimonials(),
    ]);

  return {
    props: {
      latestGiveaway,
      categories,
      products,
      testimonials,
    },
  };
}
